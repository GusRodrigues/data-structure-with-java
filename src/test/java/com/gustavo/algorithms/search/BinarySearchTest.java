package com.gustavo.algorithms.search;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static com.gustavo.algorithms.search.BinarySearch.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test cases for the Binary Search method
 * 
 * @author Gustavo
 *
 */

public class BinarySearchTest {
	
	private static int[] small;
	private static int[] medium;

	
	@BeforeAll
	public static void createArrays() {
		small = new int[200];
		medium = new int[5000];

		for (int i = 0; i < medium.length; i++) {
			if (i < small.length) {
				small[i] = i + 1;
				medium[i] = i + 1;
			} else {
				medium[i] = i + 1;
			}
		}
	}

	@Test
	public void inexistentValueReturnsMinusOne() {
		assertEquals(-1, binarySearchItem(small, 300));
		assertEquals(-1, binarySearchItem(medium, 6000));
	}
	
	@Test
	public void existingItemReturnsTheRightIndex() {
		// indexes are the value - 1.
		// index of 200 is... 199
		assertEquals(199, binarySearchItem(small, 200));
		assertEquals(199, binarySearchItem(medium, 200));
	}
	
	@Test
	public void itemInTheMiddleReturnsTheMiddleIndex() {
		// indexes are the value - 1.
		// index of 100 is... 99
		// index of 2500 is... 2499
		assertEquals(99, binarySearchItem(small, 100));
		assertEquals(2499, binarySearchItem(medium, 2500));
	}
}
