package com.gustavo.algorithms.sorting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Random;

import org.junit.jupiter.api.Test;

/**
 * @author Gustavo
 *
 */
public interface SortingTest {
	
	/** sort the supplied array in the selected sorting method
	 * 
	 * @param arr the array to be sorted
	 * @return the permuted array sorted in ascending order
	 */
	public int[] sortedArray(int[] arr);

	/**
	 * Creates a meh array of ints.
	 * 
	 * @return an in[] array of 50 elements
	 */
	static int[] mehArray() {
		HashSet<Integer> set = new HashSet<>(50);
		while (set.size() < 50) {
			set.add(new Random().nextInt(2500000));
		}

		return set.stream().mapToInt(Integer::intValue).toArray();
	}

	/**
	 * Creates a large array of integers. it should be called once.
	 * 
	 * @return an int array of 5000 elements.
	 */
	static int[] largeArray() {
		HashSet<Integer> set = new HashSet<>(5000);
		while (set.size() < 5000) {
			set.add(new Random().nextInt(2500000));
		}

		return set.stream().mapToInt(Integer::intValue).toArray();
	}

	/**
	 * Creates a very large array of integers. it should be called once.
	 * 
	 * @return an int array of 1000000 elements.
	 */
	static int[] veryLargeArray() {
		HashSet<Integer> set = new HashSet<>(1000000);
		while (set.size() < 1000000) {
			set.add(new Random().nextInt(2500000));
		}

		return set.stream().mapToInt(Integer::intValue).toArray();
	}

	@Test
	public default void testArrayOfOneElement() {
		// assemble
		int[] array = { 1 };
		// act
		int[] answer = sortedArray(array);
		// assert
		assertEquals(1, answer.length);
		assertEquals(1, answer[0]);
	}

	@Test
	public default void testArrayOfTwoElement() {
		// assemble
		int[] array = { 2, 1 };
		// act
		int[] answer = sortedArray(array);
		// assert
		assertEquals(2, answer.length);
		assertEquals(1, answer[0]);
		assertTrue(answer[0] < answer[1],
				"first element must be less than the second, but was: " + answer[0] + " and " + answer[1]);
	}

	@Test
	public default void testArrayOfThreeElement() {
		// assemble
		int[] array = { 2, 4, 1 };
		// act
		int[] answer = sortedArray(array);
		// assert
		assertEquals(3, answer.length);
		assertEquals(1, answer[0]);
		for (int i = 1; i < array.length; i++) {
			int before = answer[i - 1];
			int after = answer[i];
			String assertion = before + " must be <= than " + after;
			assertTrue(before < after, assertion);
		}
	}

	/**
	 * Test using meh array.
	 */
	public void testArrayOf50Element();

	/**
	 * Test using the large array
	 */
	public void testArrayOf50000Element();

	/**
	 * Test using the very large array.
	 */
	public void testArrayOfAMillionElement();

}
