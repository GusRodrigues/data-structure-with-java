package com.gustavo.algorithms.sorting;

import static com.gustavo.algorithms.sorting.SortingTest.largeArray;
import static com.gustavo.algorithms.sorting.SortingTest.mehArray;
import static com.gustavo.algorithms.sorting.SortingTest.veryLargeArray;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * @author Gustavo
 *
 */
public class TestQuickSort implements SortingTest {
	private static Sort sortingWay;
	private static int[] veryLargeArray;
	private static int[] largeArray;
	private static int[] smallArray;
	
	@BeforeAll
	public static void initArrays() {
		veryLargeArray = veryLargeArray();
		largeArray = largeArray();
		smallArray = mehArray();
		sortingWay = new MyQuickSort();
	}

	@Override
	public int[] sortedArray(int[] arr) {
		return sortingWay.sort(arr);
	}
	
	
	@Test @Override
	public void testArrayOf50Element() {
		// assemble
		// act
		int[] answer = sortedArray(smallArray); 
		// assert
		
		for (int i = 1; i < answer.length; i++) {
			int before = answer[i-1];
			int after = answer[i];
			String assertion = before + " must be <= than " + after + " @ index " + i;
			assertTrue(before <= after, assertion);			
		}
	}
	
	@Test @Override
	public void testArrayOf50000Element() {
		
		// act
		int[] answer = sortedArray(largeArray); 
		// assert
		
		for (int i = 1; i < answer.length; i++) {
			int before = answer[i-1];
			int after = answer[i];
			String assertion = before + " must be <= than " + after;
			assertTrue(before <= after, assertion);			
		}
	}
	
	@Test @Override
	public void testArrayOfAMillionElement() {
		
		// act
		int[] answer = sortedArray(veryLargeArray); 
		// assert
		
		for (int i = 1; i < veryLargeArray.length; i++) {
			int before = answer[i-1];
			int after = answer[i];
			String assertion = before + " must be <= than " + after;
			assertTrue(before <= after, assertion);			
		}
	}

}
