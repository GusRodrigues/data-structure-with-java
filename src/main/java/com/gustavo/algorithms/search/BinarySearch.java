package com.gustavo.algorithms.search;

/**
 * Implementation of a Binary search for some data primitives in java
 * 
 * @author Gustavo
 *
 */
public final class BinarySearch {
	/**
	 * to prevent class instantiation
	 */
	private BinarySearch() {
	}

	/**
	 * Binary search item searches through a sorted array of integers for the
	 * desired element and returns the index of the element.
	 * 
	 * If the element to be searched is not found, returns -1.
	 * 
	 * @param data the sorted array to search
	 * @param find the element to be found
	 * @return the index of the element, if present, otherwise, it will return -1
	 */
	public static int binarySearchItem(int[] data, int find) {
		// boundary indexes.
		int low = 0;
		int high = data.length - 1;
		int mid = (low + high) / 2;

		while (low <= high) {
			// case found...
			if (find == data[mid]) {
				return mid;
			}
			// midpoint shift...
			if (find < data[mid]) {
				high = mid - 1;
				mid = (low + high) / 2;
			} else {
				low = mid + 1;
				mid = (low + high) / 2;
			}
		}

		return -1;
	}

	/**
	 * Binary search item searches through a sorted array for the desired element
	 * and returns the index of the element.
	 * 
	 * If the element to be searched is not found, returns -1.
	 * 
	 * @param data the sorted array of long to search
	 * @param find the element to be found
	 * @return the index of the element, if present, otherwise, it will return -1
	 */
	public static int binarySearchItem(long[] data, long find) {
		// boundary indexes.
		int low = 0;
		int high = data.length - 1;
		int mid = (low + high) / 2;

		while (low <= high) {
			// case found...
			if (find == data[mid]) {
				return mid;
			}
			// midpoint shift...
			if (find < data[mid]) {
				high = mid - 1;
				mid = (low + high) / 2;
			} else {
				low = mid + 1;
				mid = (low + high) / 2;
			}
		}

		return -1;
	}

	/**
	 * Binary search item searches through a sorted array of doubles for the desired
	 * element and returns the index of the element.
	 * 
	 * If the element to be searched is not found, returns -1.
	 * 
	 * @param data the sorted array to search
	 * @param find the element to be found
	 * @return the index of the element, if present, otherwise, it will return -1
	 */
	public static int binarySearchItem(double[] data, double find) {
		// boundary indexes.
		int low = 0;
		int high = data.length - 1;
		int mid = (low + high) / 2;

		while (low <= high) {
			// case found...
			if (find == data[mid]) {
				return mid;
			}
			// midpoint shift...
			if (find < data[mid]) {
				high = mid - 1;
				mid = (low + high) / 2;
			} else {
				low = mid + 1;
				mid = (low + high) / 2;
			}
		}

		return -1;
	}
}
