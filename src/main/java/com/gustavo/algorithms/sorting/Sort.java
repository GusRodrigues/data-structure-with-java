package com.gustavo.algorithms.sorting;

/** All sorting classes must provide the sort behaviour
 * 
 * @author Gustavo
 *
 */
public interface Sort {
	/**
	 * Sorts an array of {@code int} in ascending order
	 * 
	 * @param arr array to be sorted
	 * @return the permuted array in ascending order
	 */
	public int[] sort(int[] arr);
	/**
	 * Sorts an array of {@code double} in ascending order
	 * 
	 * @param arr array to be sorted
	 * @return the permuted array in ascending order
	 */
	public double[] sort(double[] arr);
	/**
	 * Sorts an array of {@code long} in ascending order
	 * 
	 * @param arr array to be sorted
	 * @return the permuted array in ascending order
	 */
	public long[] sort(long[] arr);
}
