/*
   Description of this class
   
   Revision History
  		Gustavo, 2020.02.25: Created
 */
package com.gustavo.algorithms.sorting;

import java.util.Random;

/**
 * @author Gustavo
 *
 */
public class MyQuickSort implements Sort {

	@Override
	public int[] sort(int[] arr) {
		quickSort(arr, 0, arr.length - 1);
		return arr;
	}

	/**
	 * Permutes the supplied array using the QuickSorting. i and j are the
	 * boundaries of the partition.
	 * 
	 * @param arr to be sorted
	 * @param i   the first index of the array
	 * @param j   the last index of the array
	 */
	private void quickSort(int[] arr, int i, int j) {
		// if i < j keep partitioning the array
		if (i < j) {
			int partition = partition(arr, i, j);
			// left side where the elements are < pivot
			quickSort(arr, i, partition - 1);
			// right side where the elements are > pivot
			quickSort(arr, partition + 1, j);
		}
	}

	/**
	 * moves all elements less than the pivot value to the left and moves all
	 * elements bigger than the pivot to the right. Returns the corrected index
	 * position of the pivot
	 * 
	 * @param arr to be partitioned
	 * @param low   the lower limit
	 * @param high   the top limit
	 * @return the index of the pivot
	 */
	private int partition(int[] arr, int low, int high) {
		swap(arr, low, getPivot(low, high));
		int boundary = low + 1;
		// iterate between the lower limit to the high limit, inclusive
		for (int i = boundary; i <= high; i++) {
			// swap if the current index with the lowest point (i) of the array
			if (arr[i] < arr[low]) {
				swap(arr, i, boundary++);
			}
		}
		
		boundary--; // normalize the boundary back
		
		// swap to the natural position of the pivot
		swap(arr, low, boundary);
		// return the index of the pivot
		return boundary;
	}

	/**
	 * Swaps the value of the firstIndex with the Second index
	 * 
	 * @param arr         to have the swap
	 * @param firstIndex  the first element to be swaped
	 * @param secondIndex the second element to be swaped
	 */
	private void swap(int[] arr, int firstIndex, int secondIndex) {
		int temp = arr[firstIndex];
		arr[firstIndex] = arr[secondIndex];
		arr[secondIndex] = temp;
	}

	/**
	 * Generates a random number n where low <= n <= high for the pivot point
	 * 
	 * This ensure the avarege n* Lg n of the quickSort
	 * 
	 * @param low  lowest value, inclusive
	 * @param high highest value, inclusive
	 * @return a pseudo-random number N, where N is >= low and N is <= high
	 */
	private int getPivot(int low, int high) {
		int limit = (high - low) + 1;
		return new Random().nextInt(limit) + low;
	}

	@Override
	public double[] sort(double[] arr) {
		quickSort(arr, 0, arr.length -1);
		return arr;
	}
	
	/**
	 * Permutes the supplied array using the QuickSorting. i and j are the
	 * boundaries of the partition.
	 * 
	 * @param arr to be sorted
	 * @param i   the first index of the array
	 * @param j   the last index of the array
	 */
	private void quickSort(double[] arr, int i, int j) {
		// if i < j keep partitioning the array
		if (i < j) {
			int partition = partition(arr, i, j);
			// left side where the elements are < pivot
			quickSort(arr, i, partition - 1);
			// right side where the elements are > pivot
			quickSort(arr, partition + 1, j);
		}
	}

	/**
	 * moves all elements less than the pivot value to the left and moves all
	 * elements bigger than the pivot to the right. Returns the corrected index
	 * position of the pivot
	 * 
	 * @param arr to be partitioned
	 * @param low   the lower limit
	 * @param high   the top limit
	 * @return the index of the pivot
	 */
	private int partition(double[] arr, int low, int high) {
		swap(arr, low, getPivot(low, high));
		int boundary = low + 1;
		// iterate between the lower limit to the high limit, inclusive
		for (int i = boundary; i <= high; i++) {
			// swap if the current index with the lowest point (i) of the array
			if (arr[i] < arr[low]) {
				swap(arr, i, boundary++);
			}
		}
		
		boundary--; // normalize the boundary back
		
		// swap to the natural position of the pivot
		swap(arr, low, boundary);
		// return the index of the pivot
		return boundary;
	}

	/**
	 * Swaps the value of the firstIndex with the Second index
	 * 
	 * @param arr         to have the swap
	 * @param firstIndex  the first element to be swaped
	 * @param secondIndex the second element to be swaped
	 */
	private void swap(double[] arr, int firstIndex, int secondIndex) {
		double temp = arr[firstIndex];
		arr[firstIndex] = arr[secondIndex];
		arr[secondIndex] = temp;
	}


	@Override
	public long[] sort(long[] arr) {
		quickSort(arr, 0, arr.length -1);
		return arr;
	}
	
	/**
	 * Permutes the supplied array using the QuickSorting. i and j are the
	 * boundaries of the partition.
	 * 
	 * @param arr to be sorted
	 * @param i   the first index of the array
	 * @param j   the last index of the array
	 */
	private void quickSort(long[] arr, int i, int j) {
		// if i < j keep partitioning the array
		if (i < j) {
			int partition = partition(arr, i, j);
			// left side where the elements are < pivot
			quickSort(arr, i, partition - 1);
			// right side where the elements are > pivot
			quickSort(arr, partition + 1, j);
		}
	}

	/**
	 * moves all elements less than the pivot value to the left and moves all
	 * elements bigger than the pivot to the right. Returns the corrected index
	 * position of the pivot
	 * 
	 * @param arr to be partitioned
	 * @param low   the lower limit
	 * @param high   the top limit
	 * @return the index of the pivot
	 */
	private int partition(long[] arr, int low, int high) {
		swap(arr, low, getPivot(low, high));
		int boundary = low + 1;
		// iterate between the lower limit to the high limit, inclusive
		for (int i = boundary; i <= high; i++) {
			// swap if the current index with the lowest point (i) of the array
			if (arr[i] < arr[low]) {
				swap(arr, i, boundary++);
			}
		}
		
		boundary--; // normalize the boundary back
		
		// swap to the natural position of the pivot
		swap(arr, low, boundary);
		// return the index of the pivot
		return boundary;
	}

	/**
	 * Swaps the value of the firstIndex with the Second index
	 * 
	 * @param arr         to have the swap
	 * @param firstIndex  the first element to be swaped
	 * @param secondIndex the second element to be swaped
	 */
	private void swap(long[] arr, int firstIndex, int secondIndex) {
		long temp = arr[firstIndex];
		arr[firstIndex] = arr[secondIndex];
		arr[secondIndex] = temp;
	}
}
